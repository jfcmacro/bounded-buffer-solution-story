---
repository: https://github.com/jfcmacro/bounded-buffer.git
story_name: Bounded Buffer Solution Story
story_language: java
tag: non-solution-v1
origin: git@gitlab.com:jfcmacro/bounded-buffer-solution-story-java.git
---

# Bounded Buffer Solution Story

**Estimated reading times**: Unknown

## Story Outline

Concurrency is one of the most critical issues in programming today. A lot of programmers avoid that subject and sometimes ignore it completely.

This story goes about a classic concurrent problem: Bounded-Buffer. This problem was proposed and solved by Edge W. Dijkstra; this shows how two processes, producer and consumer, communicate using a limited-size buffer. We start to offer a first approach where the concurrent solution is avoided, but this shows us the relevant part that we have to take into account when we manage concurrent problems, in particular, identify which ones are the parts of the *critical section*. Then we move to the first solution, which uses low-level primitives found in Java Programming Languages: `synchronized` methods (or `synchronized` blocks), with `wait` and notifies ways that mimic the behavior of condition variables.

Next, we show another solution to the problem but use *Semaphores* as a mechanism to manage concurrency. A Semaphore is an integer counter that increments (`release`) and decrements (`acquire`) in an atomic way. When a decrement value is zero, the process or thread invokes that operation is suspended until another process or thread increments the counter.

Finally, we show a final solution using locks and conditions, which is sometimes ...

---
focus: 
---

### Bounded Buffer Problem

Edge W. Dijkstra proposed a concurrent problem where two processes: producer and consumer, both communicate by passing elements from the producer to the consumer using a shared bounded buffer. The producer produces an instance of an element and puts it on the buffer, and the consumer takes an available element from it and consumes it. Both processes can do its task at the same time. 


The producer and consumer are independent activities represented as processes or threads. The bounded buffer is a structure that contains a limited number of elements produced by the producer and consumed by the consumer. We focus on the buffer first.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBuffer.java
---

### Interface `BoundedBuffer`

The `BoundedBuffer` represents a service that stores and retrieves a set of elements of any kind. The service to store is represented by the method `put` that receives and stores it. The retrieved service is represented by the method `get,` which retrieves a value stored on the `BoundedBuffer`. There is a static method, `no-know-yet`, which facilitates the creation of an array of any type. This will use on the constructor of implementation classes of this interface.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferSimpleImpl.java
---

### A Naive Solution (`BoundedBufferSimpleImpl`)

In this solution, we implement the interface `BoundedBuffer` with `BoundedBufferSimpleImpl`. This is a generic implementation; as you can see with the type of the array variable `buffer`, it is a buffer of any type `T`. The constructor creates a new array using the interface's static function. We also need three different variables: `in` indicates where a new element will be stored at the internal `buffer`, `out` indicates where the elements are taken from the consumer, `counter` indicates the number of elements stored at the `buffer` by the producer and ready to be retrieved by the consumer.

Implementing the `put` method, we first check that the `buffer` is not complete, and if this is the case, the producer will wait until at least an element is removed from the `buffer`. Then put the element in the position `in` and move a position that index (here we are using a circular buffer; when `in` reaches the maximum size -`buffer.length`- it start again with the index `0`), and increase the `counter` variable.

Implementing the `get` method, we first check that the `buffer` is empty, and if this is the case, the consumer will wait until at least an element is put on the `buffer`. Then remove the element in the position `out` and move a position that index (`out` is also a circular index), and decrease the `counter` variable.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLowLevelImpl.java
---

### Contest Condition

The previous solution is not correct. When we stare at our solution, we wonder where the error lurks. A particular condition comes up when your code is executed simultaneously by different processes or threads, named *Contest Condition*.  

To understand when a contest condition appears, we need to understand how some instructions are translated and executed. Take, for instance, the instruction: `counter++`.  It's translated into:

```bytecode
0: getfield #4 // Load on stack field counter
3: iconst_1    // Load on stack const integer 1
4: iadd        // take the two first elements at stack's top and added lefting the result on the stacks's top
5: putfield #4 // Store the result on the field counter
```

The instruction `counter--` is translated into:

```bytecode
0: getfield #4 // Load on stack field counter
3: iconst_1    // Load on stack const integer 1
4: isub        // take the two first elements at stack's top and substract lefting the result on the stacks's top
5: putfield #4 // Store the result on the field counter
```

Suppose that `counter` has `3` as the current value. Both processes or threads (producer and consumer) are executing the statements `counter++` and `counter--` at the same time: 

```text
t_0: c0: getfield #4 // c_stack[3,...]
t_1: p0: getfield #4 // p_stack[3,...]
t_2: p3: iconst_1    // p_stack[1,3,...]
t_3: c3: iconst_1    // p_stack[1,3,...]
t_4: p4: iadd        // p_stack[4,...]
t_5: p4: isub        // p_stack[2,...]
t_6: c5: putfield #4 // counter <- 2
t_7: p5: putfield #4 // counter <- 4
```

In the above possible sequence, the final value of the `counter` variable is `4,` or the last two statements are swapped, and the final value of the `counter` is  `2`. But the expected value is `3`. This happens because the execution between both higher-level statements is intermixed on the low-level, the order is mixed, and there is no guarantee to obtain the expected value.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLowLevelImpl.java
---

### Learning from Our Naive Solution

* We must ensure that any process or thread accessing a shared variable o set of shared variables, that process or thread must access those variables in an exclusive way without the interference of other processes or threads.
* There are conditions inside a critical section that temporarily removes exclusive access. The producer checks if the `buffer` is full or when the consumer checks if the `buffer` is empty.
* There are situations inside a critical section that temporarily remove processes or threads that could be readmitted again to the critical section, a consumer waiting in an empty `buffer` when a producer has put a new element into the `buffer`,  or a producer waiting in a full `buffer`.

---
focus:
tag: low-level-sln-v1
---

### Using Low-Level Primitives Concurrent Mechanism in Java

Java was designed with concurrency in mind. The programming language was designed to implement a reduced form of Monitors, a concurrent mechanism to manage concurrent structures. 

Every object in Java has an internal lock that enables you to implement exclusive access, which the programmer can use in your program with two different constructors: `synchronized` method and `synchronized` block. When a thread invokes the `synchronized` method, that thread acquires the lock of the object owner of the `synchronized` method while executing that method.

As every object has its lock, an object also provides two sets of methods: `wait`, `notify` (`notifyAll`). The `wait` method provides a form in that a thread releases a lock temporarily while a specific condition is set. The `notify` method provides a form to signal a thread awaiting a thread for a specific condition. 

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferSimpleImpl.java
---

### Implementing Interface `BoundedBuffer` with Low-Level Primitives

To extend the interface `BoundedBuffer`, first, we need to control access to the shared variables of our model: `in`, `out`, `counter`, and `buffer`. As you can see, each overridden method (`put`, `get`) are `synchronized` method. This guarantees that only one thread is inside an instance of `BoundedBufferSimpleImpl` when executing any of those methods. The others threads are waiting outside, expecting to get into.

Second, we need to check each condition: `buffer` is full or empty. Look at the inside of the method `put`,  and the `while` statement checks when the `buffer` is full. When this happens, the invoking thread waits for a signal that indicates the condition has changed; when the thread is waiting, the lock is freed, enabling other threads to access the object. When the signal is received, and the thread gets up, reacquire the object lock (this doesn't happen immediately), and it checks again if the condition has changed; it so goes on, and it doesn't wait for another signal. When the thread goes on, it puts the element into the `buffer` and increases the `counter`, and finally, sends a signal to any consumer waiting when the `buffer` is empty.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferSimpleImpl.java
---

### Low-Level Primitives

* The low-level primitives came from the first versions of Java.
* They help to solve race conditions, data conditions, and data races.
* If you're using `synchronized` methods or `synchronized` blocks, Java doesn't give support to test if there is a thread belonging to the object lock, and you have to wait indefinitely for the object's lock release.
* Low-Level Primitives only support one generic form of condition variable that has to overuse to manage the different condition variables, for instance, in our productor-consumer solution we have to manage two different conditions (full buffer and empty buffer), with the same protocol: `wait` and `notify`, but both don't indicates which conditions is (full buffer or empty buffer).

---
focus: 
tag: semaphore-sln-v2
---

### Semaphores

The Semaphore is a counter, which has two operations: originally named by its author `P()`(*Proberen*, to prove, to test, to acquire, to decrease, to wait) and `V()`(*Vehorgen*, to increment, to release, to signal). In Java, the operations are named: `acquire` and `release`, respectively. `acquire` decrease the semaphore value in an atomic way, remember the *bytecode* translation of the `counter++` statement; when the value of the semaphore is negative, the thread is put in an awaiting state.  `release` increases the value of the semaphore; if there is any other thread in an awaiting state, it is released.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferSemaphoreImpl.java
---

### Implementing Interface `BoundedBuffer` with Semaphores

The solution uses three semaphores: `mutex` (*Mutual Exclusion*) semaphore indicates that will manage that only one thread can be manipulated shared variables, `full` semaphore indicates how many free slots of the buffer, and `empty` semaphore indicates how many elements of the `buffer` are without element.

A `mutex` semaphore is used to control a *Mutual Exclusion* (Critical Region); this requires a particular pattern to manage the access of shared variable:

```java
try {
    mutex.acquire();
    // Critical Section
    // Access to shared variables
}
finally {
    mutex.release();
}
```

 The other two Semaphores: `full` and `empty` are used in conjunction; when you `acquire` one, you must `release` the other, and vice versa.  For the implementation of the method `get()`, for instance, you follow that pattern:

```java
empty.acquire();
// ...
full.release();
```

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferSemaphoreImpl.java
---

### Semaphores Conclusions

* A semaphore is a more generic mechanism for managing mutual exclusion and signals.
* Almost every Operating System and Programming Language offers an API or library with support for Semaphores.

---
focus: 
tag: lock-sln
---

### Locks 

* Locks are a higher-level concurrent mechanism.
* Locks support fairness, meaning each thread has the same probability and will choose at the same rate.
* Locks are more extensible; for instance, you can immediately back out of a lock-acquisition attempt when a lock isn't available.
* Lock acquisition and release are more flexible than synchronized methods and blocks.

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLockImpl.java
---

### Implementing Interface `BoundedBuffer` with Lock

This solution only requires a Lock which handles exclusive access to the shared variables: `buffer`, `counter`, `in`, `out`. This is the protocol to enter a critical section.

```java
lock.acquire();
try {
    // Shared variables
}
catch (Exceptions ...) {
    // Manages exceptions
}
finally {
    lock.unlock();
}
```

But Lock not only tries with mutual exclusion, it can manage different variable conditions depending on the context of the solution. In this case, our solution has to manage two conditions: `full` `buffer` and `empty` `buffer`.  From the Lock, you can [create the two conditions](src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLockImpl.java:17-23), and using on each condition, the method `await()` when a thread must wait on a particular condition, and using the method `signal()` to indicate to the thread that the condition is over. 

---
focus: src/main/java/com/epam/rd/boundedbuffer/BoundedBufferLockImpl.java
---

### Conclusion

* Java is a programming language designed with concurrency in mind.
* Java provides different levels of mechanisms to manage concurrency, since low-level mechanisms are based on `synchronized`methods and `synchronized` blocks, to Locks.
* Semaphores are a mechanism that can be found on many operating systems and programming languages.
* In Java, Locks is more flexible and useful.
